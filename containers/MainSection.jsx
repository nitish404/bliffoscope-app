import React, { Component, PropTypes } from 'react';
import { connect } from "react-redux";
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider'
import Paper from 'material-ui/Paper'
import AppBar from 'material-ui/AppBar'
import RaisedButton from 'material-ui/RaisedButton'
import RadioButtonGroup from 'material-ui/RadioButton/RadioButtonGroup'
import RadioButton from 'material-ui/RadioButton'
import Slider from 'material-ui/Slider'
import Dialog from 'material-ui/Dialog'
import CircularProgress from 'material-ui/CircularProgress';
import {submitPattern, submitPatternSuccess, submitPatternFailure} from '../actions/bliffoscope';
import { bindActionCreators } from 'redux'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';

const styles = {
  defaultStyle : {
    width: 300,
    marginLeft: 20
  },
  paperStyle: {
    height : "50%", 
    width : "90%", 
    margin: 10, 
    textAlign: 'center', 
    display: 'inline-block',
    padding: 10, 
    alignItems: 'center'
  },
  paperStyleHalf: {
    height : "50%", 
    width : "45%", 
    margin: 5, 
    textAlign: 'center', 
    display: 'inline-block',
    alignItems: 'center'
  },
  radioButton: {
    marginBottom: 10,
    width: "50%"
  },
  slider: {
    marginBottom: 5,
    width: "100%"
  },
  spanStyle: {
    fontWeight: "bold"
  },
  tablePaper: {
    height: 300,
    width: '100%',
    overflow: 'auto'
  }
}

let actionDispatch = null;

class MainSection extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    tolerance: 50,
    strategy: "brute",
    input: '',
    pattern: '',
    inputErrorText: '',
    patternErrorText: '',
    patternMatchDialogOpen: false,
    patternMatchComplete: false
  };

  onSubmitPattern = () => {
    const {input, pattern, strategy, tolerance} = this.state;
    if(pattern === ''){
      this.setState({patternErrorText: 'Pattern is required'});
    } else if(input === ''){
      this.setState({inputErrorText: 'Input is required'});
    } else {
      this.setState({patternMatchDialogOpen: true})
      this.props.submitPattern(actionDispatch, input, pattern, strategy, tolerance);
    }
  }
  
  handleToleranceChange = (event, value) => {
    this.setState({tolerance: value});
  };

  handleStrategyChange = (event, value) => {
    this.setState({strategy: value});
  };

  handlePatternFileSelect = (evt) => {
    const file = evt.target.files[0]; // FileList object
    const reader = new FileReader();
    const thisInstance = this;
    reader.onload = function(event) {
        const contents = event.target.result;
        thisInstance.setState({pattern: contents});
    };
    reader.onerror = function(event) {
        console.error("File could not be read! Code " + event.target.error.code);
    };
    reader.readAsText(file);
  }

  handleInputFileSelect = (evt) => {
    const file = evt.target.files[0]; // FileList object
    const reader = new FileReader();
    const thisInstance = this;
    reader.onload = function(event) {
        const contents = event.target.result;
        thisInstance.setState({input: contents});
    };
    reader.onerror = function(event) {
        console.error("File could not be read! Code " + event.target.error.code);
    };
    reader.readAsText(file);
  }

  handlePatternChange = (event, value) => {
    this.setState({pattern: value, patternErrorText:''});
  };

  handleInputChange = (event, value) => {
    this.setState({input: value, inputErrorText:''});
  };

  handleDialogClose = () => {
    this.setState({
      patternMatchDialogOpen: false,
      result: null,
      error: null,
      patternMatchComplete: false
    });
  };

  componentWillReceiveProps(nextProps) {
    if(nextProps.bliffoscopeReducer){
      this.setState({
        result: nextProps.bliffoscopeReducer.result,
        error: nextProps.bliffoscopeReducer.error,
        patternMatchComplete: nextProps.bliffoscopeReducer.patternMatchComplete
      })
    }
  }

  render() {
    const { bliffoscopeReducer, submitPattern, submitPatternSuccess, submitPatternFailure} = this.props;
    let dialogContent = <CircularProgress size={100} thickness={5} />;
    if (this.state && this.state.error) {
      dialogContent = <span> {this.state.error} </span>
    } else if(this.state && this.state.result){
      dialogContent = <Paper style={styles.tablePaper}>
        <Table bodyStyle={{overflowY: 'auto'}}>>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn>X Position</TableHeaderColumn>
              <TableHeaderColumn>Y Position</TableHeaderColumn>
              <TableHeaderColumn>Errors</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox ={false}>
          {
            this.state.result.map(function(resTmp, index){
              return <TableRow key={index}>
                <TableRowColumn>{resTmp.gridLocation.x}</TableRowColumn>
                <TableRowColumn>{resTmp.gridLocation.y}</TableRowColumn>
                <TableRowColumn>{resTmp.errors}</TableRowColumn>
              </TableRow>;
          })
          }
          </TableBody>
        </Table>
      </Paper>
    }
    const actions = [
      <RaisedButton
        label="OK"
        primary={true}
        disabled={!this.state.patternMatchComplete}
        onClick={this.handleDialogClose}
      />
    ];
    return (
      <div>
      <AppBar
        title={<span>Bliffoscope</span>}
        showMenuIconButton = {false}
        iconElementRight={<RaisedButton label="Get Matches" onClick={this.onSubmitPattern}/>}
      />
      <span style={styles.spanStyle}> Select algorithm and its error tolerance: </span><br />
      <Paper zDepth={1} style={styles.paperStyleHalf}>
        <RadioButtonGroup name="strategy" defaultSelected="brute" onChange={this.handleStrategyChange}>
          <RadioButton
            value="brute"
            label="Brute Force"
            style={styles.radioButton}
          />
          <RadioButton
            value="sum"
            label="Sum Algorithm"
            style={styles.radioButton}
          />
        </RadioButtonGroup>
      </Paper>
      
      <Paper zDepth={1} style={styles.paperStyleHalf}>
        <Slider
            min={0}
            max={100}
            step={1}
            value={50}
            onChange={this.handleToleranceChange}
            style={styles.slider}
          />
          <span>{'Error tolerance is: '}</span>
          <span>{this.state.tolerance}</span>
          <span>{' from a range of 0 to 100 inclusive'}</span>
        </Paper>
      <br />
      <span style={styles.spanStyle}> Enter OR Upload Pattern : </span>
      <RaisedButton
        containerElement='label'>
        <input type="file" id="patternFile" onChange={this.handlePatternFileSelect}/>
      </RaisedButton>
      <Paper zDepth={3} style={styles.paperStyle}>
        <TextField
          hintText="Pattern"
          multiLine={true}
          fullWidth={true}
          rowsMax={5}
          rows = {5}
          value= {this.state.pattern}
          onChange ={this.handlePatternChange}
          errorText={this.state.patternErrorText}
        />
      </Paper>
      <br />
      <span style={styles.spanStyle}> Enter OR Upload Input :</span>
      <RaisedButton
        containerElement='label'>
        <input type="file" id="inputFile" onChange={this.handleInputFileSelect}/>
      </RaisedButton>
      <Paper zDepth={3} style={styles.paperStyle}>
        <TextField
          hintText="Input"
          fullWidth={true}
          multiLine={true}
          rowsMax={7}
          rows = {5}
          value = {this.state.input}
          onChange ={this.handleInputChange}
          errorText={this.state.inputErrorText}
        />
      </Paper>
      <Dialog
          title="Matching Patterns"
          modal={true}
          open={this.state.patternMatchDialogOpen}
          style={styles.paperStyle}
          actions={actions}
        >
        {dialogContent}
      </Dialog>
      </div>
    );
  }
}

MainSection.propTypes = {
  submitPattern: PropTypes.func.isRequired,
  submitPatternSuccess: PropTypes.func.isRequired,
  submitPatternFailure: PropTypes.func.isRequired
};

function mapStateToProps(state){
  return state
}

function mapDispatchToProps(dispatch) {
  actionDispatch = dispatch;
  return {
    submitPattern: bindActionCreators(submitPattern, dispatch),
    submitPatternSuccess: bindActionCreators(submitPatternSuccess, dispatch),
    submitPatternFailure: bindActionCreators(submitPatternFailure, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainSection);
