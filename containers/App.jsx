import React, { Component, PropTypes } from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import theme from '../src/material_ui_raw_theme_file'
import {submitPattern, submitPatternSuccess, submitPatternFailure} from '../actions/bliffoscope';
import MainSection from './MainSection'

class App extends Component {
  constructor(props) {
    super(props);
  }
  
  state = {
    input: "+ +",
    pattern: "+ +",
    tolerance: 1,
    algo: "brute"
  }
  render() {
    const { submitPattern, submitPatternSuccess, submitPatternFailure } = this.props;
    return (
      <div>
        <MuiThemeProvider muiTheme={theme}>
          <div>
           <MainSection {...this.props}/>
          </div>
        </MuiThemeProvider>
      </div>
    );
  }
}

App.propTypes = {
  submitPattern: PropTypes.func.isRequired,
  submitPatternSuccess: PropTypes.func.isRequired,
  submitPatternFailure: PropTypes.func.isRequired
};

function mapStateToProps(state){
  return state
}

function mapDispatchToProps(dispatch) {
  return {
    submitPattern, submitPatternSuccess, submitPatternFailure
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
