# react-redux-material_ui-boilerplate
A boilerplate for React + Redux + Material UI + ES6 syntax applications. This boilerplate includes the following tools and frameworks:

* [React](https://facebook.github.io/react/)
* [Redux](http://redux.js.org/)
* [Material UI](http://material-ui.com/#/)
* [webpack](https://webpack.github.io/)
* [Babel](https://babeljs.io/)
* [ESLint](http://eslint.org/)
