import {SUBMIT_PATTERN, SUBMIT_PATTERN_SUCCESS, SUBMIT_PATTERN_FAILURE} from '../constants/ActionTypes';

const initialState = {
};

export default function bliffoscopeReducer(state = initialState, action) {
  switch (action.type) {
  case SUBMIT_PATTERN:
    return state;

  case SUBMIT_PATTERN_SUCCESS:
    return {
      ...state,
      result: action.result,
      error: null,
      patternMatchComplete: true
    };

  case SUBMIT_PATTERN_FAILURE:
    return {
      ...state,
      error: action.error,
      result: null,
      patternMatchComplete: true
    };

  default:
    return state;
  }
}
