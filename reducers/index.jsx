import { combineReducers } from 'redux';
import bliffoscopeReducer from './bliffoscope';

const rootReducer = combineReducers({
  bliffoscopeReducer: bliffoscopeReducer
});

export default rootReducer;
