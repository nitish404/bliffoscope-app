import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers';
import  createLogger  from 'redux-logger'
const logger = createLogger();

export default function configureStore(initialState) {

  const store = createStore(
    rootReducer,
    applyMiddleware(logger),
    initialState
    
    //window.devToolsExtension ? window.devToolsExtension() : undefined
  );

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers');
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}
