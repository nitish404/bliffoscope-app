import * as types from '../constants/ActionTypes';

export function submitPattern(actionDispatch, input, pattern, strategy, tolerance) {
  postData('http://localhost:8080/bliffo/match', 
    {
      strategy,
      tolerance,
      pattern: getPayloadObject(pattern),
      input: getPayloadObject(input)
    })
  .then(data => {
    actionDispatch(submitPatternSuccess(data));
  })
  .catch(error => {
    console.log(error.message);
    actionDispatch(submitPatternFailure(error.message));
  })
  return { type: types.SUBMIT_PATTERN};
}

export function submitPatternSuccess(data) {
  return { result: data, type: types.SUBMIT_PATTERN_SUCCESS};
}

export function submitPatternFailure(error) {
  return { error:error, type: types.SUBMIT_PATTERN_FAILURE};
}

function postData(url, data) {
  // Default options are marked with *
  return fetch(url, {
    body: JSON.stringify(data),
    cache: 'no-cache',
    headers: {
      'content-type': 'application/json'
    },
    method: 'POST'
  })
  .then(response => response.json())
}

function getPayloadObject(input) {
  let result  = [];
  let lines = input.split("\n");
  for (let index = 0; index < lines.length; ++index) {
    let chars = lines[index].split("");
    let tmpResult = [];
    for (let index2 = 0; index2 < chars.length; ++index2) {
      if(chars[index2] === " "){
        tmpResult.push({"value": false});
      } else {
        tmpResult.push({"value": true});
      }
    }
    if(tmpResult.length > 0){
      result.push(tmpResult);
    }
  }
  return result;
} 